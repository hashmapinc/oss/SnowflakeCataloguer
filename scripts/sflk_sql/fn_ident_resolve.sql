/*
* Ref : https://docs.snowflake.com/en/sql-reference/identifiers-syntax.html
* Based on Snowflake identifier requirements, we could have db object (table,
* columns etc..) with spaces and other ascii characters.
*
* When these characters (for ex: ~, # ...) are part of the column name they
* are typically used with quotes in the sql queries. For ex:
*
* SELECT COLA, "COL B", "COL#C" from "TABLE SPACED NAMES";
*
* To handle this smoothly, we first replace these non-alphanumeric characters
* with its corresponding ascii codes, so that the quotes can be removed. hence
* during our process we would convert the above example to :
*
* SELECT COLA, COL_32_B, COL_35_C from TABLE_32_SPACED_32_NAMES;
*
* This function is used for resolution on table/column names instead of queries.
*/
CREATE OR REPLACE FUNCTION FN_IDENT_RESOLVE(IDENT STRING)
    RETURNS STRING
    LANGUAGE JAVASCRIPT
AS
$$
    function resolve_ident(ident) {
      var t=ident;
      var rx=/\w/;

      if (t === undefined)
        return null;

      var ct = t.split('').map(x => {
          if(rx.exec(x) != null)
              return x;
          return '_'+x.charCodeAt(0)+'_';
      }).join('');

      return ct;
    }

    return resolve_ident(IDENT);
$$
;
