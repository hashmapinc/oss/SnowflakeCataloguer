/*
* Queries like below:
*   SELECT C.COL_1, D.DCOL2
*   FROM TBL C, TBL2 AS D;
* Adopts alias, by back referencing the alias (C), we can determine the table
* 'TBL'. This function helps in doing these back references and returning the
* results in a array like :
* [ 'DB.SCH.TBL::COL_1' , 'DB.SCH.TBL2::DCOL2']
*/
CREATE OR REPLACE FUNCTION FN_GET_COLS_BASEDON_ALIAS(P_QRY_TEXT STRING, P_DB STRING, P_SCH STRING)
    RETURNS ARRAY
    LANGUAGE JAVASCRIPT
AS
$$
    function fn_identify_alias_to_tbl(p_QRY_TEXT) {

      var qry_without_kw = p_QRY_TEXT.replace(/\sFROM\s/g,' ').replace(/\sAS\s/g,' ');
      var qry_spaced = qry_without_kw.replace(/\,/g,' , ');

      // Identify aliases by searching for pattern
      // <alias>.<potential column>
      var rx = /\s(\w+)\.(.*?)\s/g,matched;
      var alias_set = new Set();
      while(matched = rx.exec(qry_spaced)) {
          alias = matched[1];
          alias_set.add(matched[1]);
      }
      // console.log(alias_set);

      // search for duo pair words of the form
      // <potential table name> <identified alias>
      var alias_tbl_dict = {};
      for(var elem of alias_set)  {
          // console.log(' > ' + elem)
          var rx = new RegExp("\\s([\\w|\\.]+)\\s" + elem + "\\s", "g");

          matched = rx.exec(qry_spaced);
          if(matched != null){
              alias_tbl_dict[elem] = matched[1];
              // console.log('-------- ' + matched[1]);
          }
      }
      //console.log(alias_tbl_dict);
      var q_tkns = JSON.stringify(alias_tbl_dict);
      return q_tkns;
    }

    function fn_get_columns_with_tbl(p_alias_tbl_map, p_QRY_TEXT, p_DB, p_SCH) {

      var alias_tbl_map = JSON.parse(p_alias_tbl_map);
      // console.log(alias_tbl_map);

      var qry_without_kw = p_QRY_TEXT.replace(/\sFROM\s/g,' ').replace(/\sAS\s/g,' ');
      var qry_spaced = qry_without_kw.replace(/\,/g,' , ');

      var fq_col = []
      for (const alias in alias_tbl_map){
          // extract table name (remove db & sch if present)
          var x = alias_tbl_map[alias];
          var dot_cnt = (x. match(/\./g) || []).length;
          var tbl_referenced = x;
          if ((dot_cnt == 0) && (p_DB !== undefined) && (p_SCH !== undefined)) {
              tbl_referenced = p_DB + '.' + x;

          } else if ((dot_cnt == 1) && (p_DB !== undefined) ) {
              tbl_referenced = p_DB + '.' + x;

          }
          // console.log(' TBL : ' + tbl_referenced);

          // extract all tokens which has the alias
          var rx = new RegExp("\\s" + alias + "\\.(.*?)\\s","g");
          var matched;
          while(matched = rx.exec(qry_spaced)){
              var col = matched[1];

              // remove all symbols (need to preserve '.')
              // var col_2 = col.replace(/[\(,\),\,,+,\-,<,>,=,\',\",:,\*,%]/g, ' ');
              var col_2 = col.replace(/[\W]/g, ' ');

              var c3 = tbl_referenced +'::'+ col_2;
              // console.log(matched);
              fq_col.push(c3.trim());
          }
      }

      var dist_fq_cols = Array.from(new Set(fq_col));
      return dist_fq_cols;
    }

    var alias_tbl_map = fn_identify_alias_to_tbl(P_QRY_TEXT);
    return fn_get_columns_with_tbl(alias_tbl_map, P_QRY_TEXT, P_DB, P_SCH);

$$
;
