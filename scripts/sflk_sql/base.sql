

/*
* We start of with defining a custom role, which will be used for processing.
* We also create a schema so that we can store our processing results etc..
*/
use role sysadmin;
use warehouse demo_wh; //CHANGE appropriately to your env.

create or replace role sflk_cataloguer
    comment = 'Used for processing on meta data from SNOWFLAKE.ACCOUNT_USAGE views';

grant usage on warehouse demo_wh  TO ROLE sflk_cataloguer;

create or replace transient schema util_db.sflk_catalog
    WITH MANAGED ACCESS
    DATA_RETENTION_TIME_IN_DAYS = 1
    COMMENT = 'Used for catalog snowflake usage activities'
    ;

grant ALL PRIVILEGES ON SCHEMA util_db.sflk_catalog TO ROLE sflk_cataloguer;

  // ==========================================================

/*
* By default, the SNOWFLAKE database is available only to the ACCOUNTADMIN role.
* To enable other roles to access the database and schemas, and query the views,
* a user with the ACCOUNTADMIN role must grant the following data sharing privilege
* to the desired roles:
*    IMPORTED PRIVILEGES
* REF : https://docs.snowflake.com/en/sql-reference/account-usage.html#enabling-account-usage-for-other-roles
*/

use role accountadmin;
grant IMPORTED PRIVILEGES ON DATABASE snowflake TO ROLE sflk_cataloguer;
