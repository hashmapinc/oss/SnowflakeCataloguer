# SnowflakeCataloguer - Data cataloging Snowflake 
  
  _A simple opinionated approach to data-cataloging, data-discovery of Snowflake; processed and served end-to-end in Snowflake._

## Links
 - [Design Approach](./docs/Design_approach.md)
 - [Installation](./docs/Installation.md)
 - [Data Processing](./docs/Data_processing.md)
 - [Demo Execution](docs/videos/demo_execution.mp4)

---
Your company has had a successful implementation of Snowflake and various departments have embraced the migration and started the adoption. You have data migrated from multiple data warehouses (Netezza, Teradata); Hadoop; databases. And probably you are also ingesting data from 3rd party vendor APIs. Your Snowflake account has probably 10 databases, each with 5 or more schema and probably 1000s of tables, columns, and views. There are multiple users across various departments running queries and executing jobs and serving various business needs.
Then comes the ask, you need to access the data inventory in Snowflake and determine 
- Who is using what data?
- How are the tables and views related to each other?
- When was the table last loaded? Is it being used?
- Which columns are important in a table and which columns are least used?

To answer these and more, the general approach is to have an enterprise-wide data catalog. A commercial offering like Alation, Collibra, Dataedo, etc is typically implemented. Open source products like Lyft's Amundsen, Magda, Airbnb's Dataportal, Netflix's Metacat, WeWork's Marquez exist.

However, not all adoption is the same. Based on your enterprise, you might have already adopted a vendor (ex: Collibra) and you need to setup/configure/integrate with the solution. This would involve working with multiple teams etc… While these adoptions are certain enterprise-wide, not all enterprises are the same. Smaller/medium-sized companies do not have the bandwidth to host and maintain such services. Sometimes a commercial offering (like [TreeSchema](https://treeschema.com)) would need approvals etc to be adopted.

But what if you could implement a simplistic approach that you want to catalog/data discovering of just Snowflake and you can achieve with the capabilities of what Snowflake offers? What do I mean?

- [Snowflake Account Usage](https://medium.com/r/?url=https%3A%2F%2Fdocs.snowflake.com%2Fen%2Fsql-reference%2Faccount-usage.html%23account-usage) has views that give insights into various objects & activities.

- [Query History View](https://medium.com/r/?url=https%3A%2F%2Fdocs.snowflake.com%2Fen%2Fsql-reference%2Faccount-usage%2Fquery_history.html%23query-history-view) lists all the queries issued by various actors, we can deconstruct the queries and determines the tables, views, and related columns.

- [JavaScript UDF](https://medium.com/r/?url=https%3A%2F%2Fdocs.snowflake.com%2Fen%2Fsql-reference%2Fudf-js.html%23javascript-udfs) offers the ability to run inside Snowflake with a well-known widely adopted language.

- The warehouse offers an isolated serverless compute option for the data to be processed and stored in Snowflake storage.

What if, we store the results in tables, and we can build views, that answers query like:
Which database, schema, tables/views are actively used; hence critical of nature?
If we need to drop table-A, what other tables will be affected? and more. 

The good news is that this approach is doable. An honorable mention is a blog from UBER on this implementation: [Queryparser, an Open Source Tool for Parsing and Analyzing SQL](https://medium.com/r/?url=https%3A%2F%2Feng.uber.com%2Fqueryparser%2F). In my previous article, [Housekeeping in Snowflake with SQL and DBT](https://medium.com/hashmapinc/housekeeping-in-snowflake-with-sql-and-dbt-a50d7448d4b1), I have adopted this approach too.

## Approach
1. Copy the Snowflake. Account Usage views into a database & schema.
2. Filter out records that currently offer fewer values. Like Show/Use/Describe commands etc.
3. Cleanse, parse and tokenize the query text from the Query_History table
4. Join back with the "Tables" & "Views" table to identify the tables, views, database, and schema used.
5. Build views/materialized table on the above table that offers various insights related to data catalog & data discovery.

The above approach is not necessarily meant for real-time needs. You can typically do this processing at the beginning of the workday or on a demand basis.

Based on processing around 1 million records from my lab, an XS warehouse-size was good enough for the complete processing. End to end the process took around 5 min and less.
The materialized tables are all transient, as we can recreate the entire content.

## Views
Here are some of the views that has been implemented:

- **TABLES_W_USAGE_COUNT** : This is a list of tables/views reflecting on how often they have been used in various queries and most recent time when the table was accessed.
![](./docs/images/TABLES_W_USAGE_COUNT.png)

- **TABLE_NEIGHBOURS** : Most typical typical queries use 2 or more tables. By disecting the queries, as implemented here, we could determine if TABLE-A has a close relationship with TABLE-D and how close these relationship is in comparison with TABLE-B. The closeness (LINK_COUNT) is based of the count of distinct queries which has reference to these tables.
![](./docs/images/TABLE_NEIGHBOURS.png)

We can filter to specific table and narrow down like below:
![](./docs/images/TABLE_NEIGHBOURS_FILTERED.png)

- **TABLE_COLUMNS_USAGE** : While a table can have 5 or 50 columns not all columns might be of same importance. Most columns may not get used at all. This dataset represents which columns for a given table, end up being used in queries and the count(COL_USAGE_COUNT) determines how often. The higher the count, might give a clue of how critical the data would be. Columns not present would indicate not being used.
![](./docs/images/TABLE_COLUMN_USAGE.png)

## Final Thoughts
What I demonstrated is just a start. Through the usage of "TABLES_USAGE_COUNT" records, i was able to clean out my Snowflake environment for a start. I am sure you will find more benefits in your adoption too. A lot more can be implemented, provided there is some interest from the community.

I hope this Snowflake housekeeping guide has helped you and provided valuable insight into getting even more value from your Snowflake cloud data warehouse. It would be great to hear your thoughts and what your next set of moves is with Snowflake!

